﻿using System;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds;
using GoogleMobileAds.Api;

[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{
	#if UNITY_EDITOR
	string adUnitId = "unused";
#elif UNITY_ANDROID
    string adUnitId = "ca-app-pub-3940256099942544/5224354917";
	//string adUnitId = "ca-app-pub-2014478882248749/7489420205";
#elif UNITY_IPHONE
	string adUnitId = "";
#else
	string adUnitId = "unexpected_platform";
#endif

    private RewardBasedVideoAd rewardBasedVideoAd;

	public Button m_Button;

	void Start ()
	{    
		rewardBasedVideoAd = RewardBasedVideoAd.Instance;

		rewardBasedVideoAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;
		rewardBasedVideoAd.OnAdOpening += HandleOnAdOpening;
		rewardBasedVideoAd.OnAdClosed += HandleOnAdClosed;
		rewardBasedVideoAd.OnAdRewarded += HandleOnAdRewarded;

		if (m_Button) {
			m_Button.onClick.AddListener (ShowAd);
		}
		AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("7A30B4FDB0ED733B").Build();
        //AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("7A30B4FDB0ED733B").Build();
        rewardBasedVideoAd.LoadAd (request, adUnitId);
	}

	void Update ()
	{
		
	}
	public void ShowAd()
	{
	if (rewardBasedVideoAd.IsLoaded()) {
			rewardBasedVideoAd.Show ();
		}
	}

	public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("7A30B4FDB0ED733B").Build();
		rewardBasedVideoAd.LoadAd (request, adUnitId);
	}

	public void HandleOnAdOpening(object sender, EventArgs args)
	{
		if (GameObject.Find ("SoundManager").GetComponent<SoundManager> () != null) 
		{
			GameObject.Find ("SoundManager").GetComponent<SoundManager> ().MuteSound();
			GameObject.Find ("SoundManager").GetComponent<SoundManager> ().MuteMusic();
		}
	}

	public void HandleOnAdClosed(object sender, EventArgs args)
	{
		if (GameObject.Find ("SoundManager").GetComponent<SoundManager> () != null) {
			GameObject.Find ("SoundManager").GetComponent<SoundManager> ().MuteSound();
			GameObject.Find ("SoundManager").GetComponent<SoundManager> ().MuteMusic();
		}
	}

	public void HandleOnAdRewarded(object sender, EventArgs args)
	{
        PlayerPrefs.SetInt ("TotalCurrency",PlayerPrefs.GetInt("TotalCurrency") + 10);
	}
}