﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdControl : MonoBehaviour {

	private BannerView bannerView;
	// Use this for initialization
	void Start () {
        PlayerPrefs.SetInt("FinishedLevels", 34);
		RequestBanner ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	private void RequestBanner()
	{
		#if UNITY_EDITOR
		string adUnitId = "unused";
        #elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-3940256099942544/6300978111";
        #elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
        #else
		string adUnitId = "unexpected_platform";
        #endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		// Create an empty ad request.
		//AdRequest request = new AdRequest.Builder().Build();
		AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("7A30B4FDB0ED733B").Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
	}

    public void DestroyBanner()
    {
        bannerView.Destroy();
    }


}
