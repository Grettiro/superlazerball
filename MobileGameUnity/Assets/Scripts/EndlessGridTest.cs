﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessGridTest : MonoBehaviour {
	public GridToJson jsonWriteTest;
	public JsonToGrid jsonReadTest;
	public Grid grid;
    public bool extraSpawned = true;
    private int round = 1;
	private int currencyRound = 1;

	private void Start()
	{
        if (grid.endless) {
            for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 7; j++) {
					grid.SetCell (j, i, new GridCell (0, 0, GridCell.powerupType.Empty, GridCell.type.Empty));
			}
		}
            SpawnLine();
        }
            //jsonWriteTest.WriteGrid (grid);
		//jsonReadTest.ReadGrid ();
	}

	public void SpawnLine()
	{
        bool extraSpawn = true;
		List<int> spawnPoints = new List<int> ();
		for (int i = 0; i < 7; i++) {
			spawnPoints.Add (i);
		}
		for (int i = 0; i < spawnPoints.Count; i++) {
			int temp = spawnPoints [i];
			int randomIndex = Random.Range (i, spawnPoints.Count);
			spawnPoints [i] = spawnPoints [randomIndex];
			spawnPoints [randomIndex] = temp;
		}

		int spawnAmount = 0;
		int rand = Random.Range (0, 16);
		if (rand < 4) {
            if(currencyRound == 3)
            {
                spawnAmount = 3;
            }
            else
            {
                spawnAmount = 2;
            }
		
		} else if (rand < 3) {
			spawnAmount = 3;
		} else if (rand < 6) {
			spawnAmount = 4;
		} else if (rand < 9) {
			spawnAmount = 5;
		} else if (rand < 12) {
			spawnAmount = 6;
		} else if (rand <= 15) {
			spawnAmount = 7;
		}
		int[] spawnArray;
		spawnArray = new int[spawnAmount];
        for (int i = 0; i < 7; i++)
        {
            bool spawned = false;
            for (int j = 0; j < spawnArray.Length; j++)
            {
                if (i == spawnPoints[j])
                {
                    spawned = true;
                    if (j == 0)
                    {
                        grid.SetCell(spawnPoints[j], 9, new GridCell(0, 0, GridCell.powerupType.Empty, GridCell.type.Ammo));
                    }
                    else if (currencyRound == 3)
                    {
                        currencyRound = 0;
                        grid.SetCell(spawnPoints[j], 9, new GridCell(0, 1, GridCell.powerupType.Empty, GridCell.type.Currency));
                    }
                    else
                    {
                        grid.SetCell(spawnPoints[j], 9, new GridCell(round, 0, GridCell.powerupType.Empty, GridCell.type.Bomb));
                    }
                }
            }
            if (!spawned)
            {
                grid.SetCell(i, 9, new GridCell(0, 0, GridCell.powerupType.Empty, GridCell.type.Empty));
            }
        }
        for (int i = 0; i < 5; i++)
        {
            for (int k = 0; k < 7; k++)
            {
                if(grid.gameGrid[k,i].cellType == GridCell.type.Bomb)
                {
                    extraSpawn = false;
                }
            }
        }
        if(extraSpawn)
        {
            if(!extraSpawned)
            {
                extraSpawned = true;
                int spawnChance = Random.Range(1, 11);
                if(spawnChance < 3)
                {
                    StartCoroutine(DelayedSpawn());
                }
            }

        }
		round++;
		currencyRound++;
        
	}
    IEnumerator DelayedSpawn()
    {
        yield return new WaitForSeconds(0.5f);
        grid.LowerGrid();
        SpawnLine();
    }
}
