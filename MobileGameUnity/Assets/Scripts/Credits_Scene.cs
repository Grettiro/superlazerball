﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credits_Scene : MonoBehaviour {
	public GameObject Main_Screen;
	public GameObject CreditsScreen;
	// Use this for initialization
	void Start () {

		CreditsScreen.SetActive (false);
	}
	
	// Update is called once per frame
	public void OpenCredit () 
	{
		CreditsScreen.SetActive (true);
		Main_Screen.SetActive (false);		
	}

	public void BackMain()
	{
		CreditsScreen.SetActive (false);
		Main_Screen.SetActive (true);
	}
}
