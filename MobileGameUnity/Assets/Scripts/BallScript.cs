﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour {
	Rigidbody2D rb;
	public float multiplier;
	public ShootBall shooter;
    public bool instaKill;
    public bool piercing;
    public int doubleDamage;
    public AudioClip wallsound;
    AudioSource ballAudio;
	SoundManager soundManager;
    private float prevPositionY;
	private float prevPositionX;
    public TrailRenderer[] trails;

	ParticleColorChange[] particleColorChange;

    void Awake()
	{
		rb = GetComponent<Rigidbody2D> ();
        ballAudio = GetComponent<AudioSource>();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
    }

    void Start()
    {
        prevPositionY = rb.position.y;
		prevPositionX = rb.position.y;
        InvokeRepeating("CorrectVelocity", 1f, 6f);
		shooter = GameObject.Find ("Shooter").GetComponent<ShootBall> ();
        if(piercing)
        {
            gameObject.layer = 9;
            transform.Find("Piercing").gameObject.SetActive(true);
        }
	}

	void Update()
	{
		rb.velocity = multiplier * rb.velocity.normalized;
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            ballAudio.pitch = Random.Range(1f, 1f);
			ballAudio.PlayOneShot(wallsound, soundManager.volume);
        }
		if (collision.gameObject.tag == "Mirror") {
            ballAudio.pitch = Random.Range(0.5f, 1f);
			ballAudio.PlayOneShot(wallsound, soundManager.volume);
			shooter.mirrorCounter++;
			if (shooter.mirrorCounter == shooter.mirrorAmmo) {
				shooter.DeactivateMirror ();
				shooter.mirrorCounter = 0;
			}
		}

    }

    public void SetVelocity(Vector2 direction)
	{
		rb.AddForce (direction * multiplier);
		rb.velocity = multiplier * rb.velocity.normalized;
	}

    public void CorrectVelocity()
    {
        if (Mathf.Abs(prevPositionY - rb.position.y) <= 0.15)
        {
            rb.velocity = new Vector2(rb.velocity.x, -1.25f);
        }
		if (Mathf.Abs(prevPositionX - rb.position.x) <= 0.15)
		{
			rb.velocity = new Vector2(-1.25f, rb.velocity.y);
		}
        else
        {
            prevPositionY = rb.position.y;
			prevPositionX = rb.position.x;
        }
    }



	public void SetBallColor(string ColorUpgrade){

		if (string.IsNullOrEmpty (ColorUpgrade)) 
		{
			return;
		}

        trails[0].gameObject.SetActive(false);
        particleColorChange = GetComponents<ParticleColorChange>();
		foreach (ParticleColorChange particle in particleColorChange) {


			//particle.active = (true);

			if (ColorUpgrade == ("RedBall")) {
				particle.SetColorRed ();
				trails [1].gameObject.SetActive (true);

			} else if (ColorUpgrade == ("BlueBall")) {
				particle.SetColorBlue ();
				trails [7].gameObject.SetActive (true);
			} else if (ColorUpgrade == ("GreenBall")) {
				particle.SetColorGreen ();
				trails [3].gameObject.SetActive (true);
			} else if (ColorUpgrade == ("PinkBall")) {
				particle.SetColorPink ();
				trails [5].gameObject.SetActive (true);
			} else if (ColorUpgrade == ("PurpleBall")) {
				particle.SetColorMagenta ();
				trails [4].gameObject.SetActive (true);
			} else if (ColorUpgrade == ("YellowBall")) {
				particle.SetColorYellow ();
				trails [2].gameObject.SetActive (true);
			} else if (ColorUpgrade == ("OrangeBall")) {
				particle.SetColorOrange ();
				trails [6].gameObject.SetActive (true);
			} else if (ColorUpgrade == ("DefaultBall")) {
				particle.SetColorDefault ();
				trails [0].gameObject.SetActive (true);
			}

		}
	}
}
