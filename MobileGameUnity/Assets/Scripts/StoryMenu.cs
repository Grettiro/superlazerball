﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class StoryMenu : MonoBehaviour {

	public GameObject[] Pages;
	private int currentPage;
	public GameObject GameMode;
	public GameObject NextButton;
	public GameObject PreviousButton;
	public GameObject loadingScreen;
    public GameObject levelselect;
	public Animator storymodeAnim;
	public buttonClicked loadLevel;

	// Use this for initialization
	void Start () {

        levelselect.SetActive(false);
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void NextPressed(){

		//Pages [currentPage].SetActive (true);
		//hide current page - int 0
		//increment current page by 1 - int 1
		//show current page

		Pages [currentPage].SetActive (false);
		//Pages [currentPage].disable;

		currentPage += 1;


		if (currentPage >= Pages.Length) {
            gameObject.SetActive(false);
            levelselect.SetActive(true);
		} 

		else {
			Pages [currentPage].SetActive (true);
			storymodeAnim.SetTrigger ("On");
		}



	}

	public void PreviousPressed(){

		//hide current page 
		//decrement the current page by 1 
		//show current page

		Pages [currentPage].SetActive (false);
		//Pages [currentPage].disable;

		currentPage -= 1;

		if (currentPage < 0) {
			currentPage = 0;
			Pages [currentPage].SetActive (true);
			GameMode.SetActive (true);
			gameObject.SetActive (false);
			//SceneManager.LoadScene ("Main_Menu Scene");
		}
		else {
			Pages [currentPage].SetActive (true);
		}
			

		/*if (currentPage < 0) {
			NextButton.SetActive (false);
			PreviousButton.SetActive (false);
		} */

	}

}
