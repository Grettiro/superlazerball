﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

	MaterialColorChange[] materialColorChange;

	// Use this for initialization
	void Start () 
	{
		SetGunColor (ItemSaves.GetLastUpgradeChosenName (ItemSaves.UpgradeCategory.GunColor));
		SetGunParticlerColor(ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.BallColor));
	}

	void SetGunParticlerColor(string ColorUpgrade)
	{
		ParticleColorChange[] gunParticles = GetComponents<ParticleColorChange> ();
		foreach (ParticleColorChange particle in gunParticles) 
		{
			if(ColorUpgrade == ("RedBall")){
				particle.SetColorRed() ;

			}
			else if(ColorUpgrade == ("BlueBall")){
				particle.SetColorBlue();
			}
			else if(ColorUpgrade == ("GreenBall")){
				particle.SetColorGreen();
			}
			else if(ColorUpgrade == ("PinkBall")){
				particle.SetColorPink();
			}
			else if(ColorUpgrade == ("PurpleBall")){
				particle.SetColorMagenta();
			}
			else if(ColorUpgrade == ("YellowBall")){
				particle.SetColorYellow();
			}
			else if(ColorUpgrade == ("OrangeBall")){
				particle.SetColorOrange();
			}
		}
	}


	public void SetGunColor(string ColorUpgrade){
		materialColorChange = GetComponents<MaterialColorChange>(); 
		foreach (MaterialColorChange material in materialColorChange) {

			if(ColorUpgrade == ("RedGun")){
				material.SetColorRed() ;

			}
			else if(ColorUpgrade == ("BlueGun")){
				material.SetColorBlue();

			}
			else if(ColorUpgrade == ("GreenGun")){
				material.SetColorGreen();

			}
			else if(ColorUpgrade == ("PinkGun")){
				material.SetColorPink(); 

			}
			else if(ColorUpgrade == ("PurpleGun")){
				material.SetColorMagenta(); 

			}
			else if(ColorUpgrade == ("YellowGun")){
				material.SetColorYellow(); 

			}
			else if(ColorUpgrade == ("OrangeGun")){
				material.SetColorOrange(); 

			}
		}
	}
}
