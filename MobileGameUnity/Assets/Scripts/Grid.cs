﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class Grid : MonoBehaviour {
	public GridCell[,] gameGrid;
    public GameGridObject[,] gameGridObjects;
    public JsonToGrid loadLevel;
    public Object[] levelManager;
    private Vector3 gridStart = new Vector3(-4.502432f, -4.17f, 0);
	public Currency currency;
	public Bomb bomb;
    public Powerup upgrade;
    public Ammo ammo;
	public Bomb taunt;
    public Bomb infected;
	public Bomb portal;
	public int gridLength;
    public int bombCount;
    public bool endless;
    public int levelCounter;
    public GameObject endscreen;
    public GameObject pause;
    public GameObject Shooter;
    public GameObject NextButton;
	public GameObject RetryButton;
    public GameObject Winner;
    public GameObject Loser;
    public GameObject HighScore;
    public TextAsset testLevelAsset;
    public bool testLevel;

    void Awake()
	{
        levelManager = Resources.LoadAll("Levels", typeof(TextAsset));
		if (PlayerPrefs.GetString ("GameMode") == "endless" && !testLevel) 
		{
			endless = true;
		}
		if (PlayerPrefs.GetString ("GameMode") == "story" || testLevel) 
		{
			endless = false;
		}
        if (endless)
        {
            gridLength = 10;
            gameGrid = new GridCell[7, gridLength];
            gameGridObjects = new GameGridObject[7, gridLength];
        }
        else
        {
            gridLength = 50;
            gameGrid = new GridCell[7, gridLength];
            gameGridObjects = new GameGridObject[7, gridLength];
            LoadLevel();
			for (int i = 0; i < 10; i++) {
				for (int k = 0; k < 7; k++) {
					if (gameGrid [k, i].cellType == GridCell.type.Taunt) 
					{
						GameObject[] bombs;
						GameObject[] infecteds;
						GameObject[] portals;
						bombs = GameObject.FindGameObjectsWithTag ("Bomb");
						infecteds = GameObject.FindGameObjectsWithTag ("Infected");
						portals = GameObject.FindGameObjectsWithTag ("Portal");
						foreach (GameObject bomb in bombs) 
						{
							bomb.GetComponent<Bomb> ().immune = true;
						}
						foreach (GameObject infect in infecteds) {
							infect.GetComponent<Bomb> ().immune = true;
						}
						foreach (GameObject portal in portals) {
							portal.GetComponent<Bomb> ().immune = true;
						}
					}
				}
			}
        }
        endscreen.SetActive(false);
        HighScore.gameObject.SetActive(false);
        //loadLevel.ReadGrid(this, level);
    }


    public void SetCell(int row, int col, GridCell cell)
	{
        GameGridObject createdObject = null;
		gameGrid [row,col] = cell;
        switch (cell.cellType)
        {
		case GridCell.type.Bomb:
			Bomb cellSpaceB = bomb;
			cellSpaceB = Instantiate (bomb, new Vector3 ((gridStart.x + row * 1.5f), gridStart.y + col * 1.5f, 0), Quaternion.identity) as Bomb;
			createdObject = cellSpaceB;
			cellSpaceB.transform.Find ("bombIdle").GetComponent<Animator> ().Play ("bombAttackIdle");
			cellSpaceB.health = cell.bombValue;
			cellSpaceB.textUpdate.text = cell.bombValue.ToString();
                break;
            case GridCell.type.Currency:
                createdObject = Instantiate(currency, new Vector3((gridStart.x + row * 1.5f), gridStart.y + col * 1.5f, 0), Quaternion.identity) as GameGridObject;
                break;
            case GridCell.type.Powerup:
                Powerup cellSpaceP = upgrade;
                cellSpaceP = Instantiate(upgrade, new Vector3((gridStart.x + row * 1.5f), gridStart.y + col * 1.5f, 0), Quaternion.identity) as Powerup;
                createdObject = cellSpaceP;
                cellSpaceP.powerup = cell.powerup;
                break;
            case GridCell.type.Ammo:
                createdObject = Instantiate(ammo, new Vector3((gridStart.x + row * 1.5f), gridStart.y + col * 1.5f, 0), Quaternion.identity) as Ammo;
                break;
			case GridCell.type.Empty:
				gameGridObjects [row, col] = null;
                return;
			case GridCell.type.Taunt:
				Bomb cellSpaceT = taunt;
				cellSpaceT = Instantiate (taunt, new Vector3 ((gridStart.x + row * 1.5f), gridStart.y + col * 1.5f, 0), Quaternion.identity) as Bomb;
				createdObject = cellSpaceT;
				cellSpaceT.transform.Find ("bombIdle").GetComponent<Animator> ().Play ("bombAttackIdle");
				cellSpaceT.health = cell.bombValue;
				cellSpaceT.textUpdate.text = cell.bombValue.ToString();
				cellSpaceT.tauntBomb = true;
				break;
            case GridCell.type.Infected:
				Bomb cellSpaceI = infected;
                cellSpaceI = Instantiate(infected, new Vector3((gridStart.x + row * 1.5f), gridStart.y + col * 1.5f, 0), Quaternion.identity) as Bomb;
                createdObject = cellSpaceI;
                cellSpaceI.transform.Find("bombIdle").GetComponent<Animator>().Play ("bombAttackIdle");
                cellSpaceI.health = cell.bombValue;
				cellSpaceI.textUpdate.text = cell.bombValue.ToString();
				cellSpaceI.initialHealth = cell.bombValue;
                cellSpaceI.infectedBomb = true;
                break;
		case GridCell.type.Portal:
			Bomb cellSpaceX = portal;
			cellSpaceX = Instantiate(portal, new Vector3((gridStart.x + row * 1.5f), gridStart.y + col * 1.5f, 0), Quaternion.identity) as Bomb;
			createdObject = cellSpaceX;
			cellSpaceX.transform.Find("bombIdle").GetComponent<Animator>().Play ("bombAttackIdle");
			cellSpaceX.health = cell.bombValue;
			cellSpaceX.textUpdate.text = cell.bombValue.ToString();
			cellSpaceX.portalBomb = true;
			break;
            default:
                return;
        }
        createdObject.row = row;
        createdObject.col = col;
        gameGridObjects[row, col] = createdObject;
    }

	public void LowerGrid()
	{
		if (!endless) {
			if (!BombCount()) {
				LevelFinished ();
			}
		}
        levelCounter++;
		for (int i = 0; i < gridLength; i++) {
			for (int j = 0; j < 7; j++) {
				if (i != 0)
				{
					if (gameGridObjects[j, i] != null)
					{
						//StartCoroutine (Transition (j,i,0.1f));
						//gameGridObjects [j, i].transform.position = Vector3.Lerp (gameGridObjects [j, i].transform.position, new Vector3 (gameGridObjects [j, i].transform.position.x, gameGridObjects [j, i].transform.position.y - 1.5f, 0), 0.5f * Time.deltaTime);
						//gameGridObjects[j, i].transform.position = new Vector3(gameGridObjects[j, i].transform.position.x, gameGridObjects[j, i].transform.position.y - 1.5f, 0);
						gameGridObjects[j, i].col -= 1;
						gameGridObjects[j, i - 1] = gameGridObjects[j, i];
						gameGridObjects[j, i] = null;
					}
					gameGrid[j, i - 1] = gameGrid[j, i];
				}
			}
		}
		for (int i = 0; i < gridLength; i++) {

			for (int j = 0; j < 7; j++) {
				if (i != 0)
				{
					if (gameGridObjects[j, i] != null)
					{
						StartCoroutine (Transition (j,i,0.1f));
						//gameGridObjects [j, i].transform.position = Vector3.Lerp (gameGridObjects [j, i].transform.position, new Vector3 (gameGridObjects [j, i].transform.position.x, gameGridObjects [j, i].transform.position.y - 1.5f, 0), 0.5f * Time.deltaTime);
						//gameGridObjects[j, i].transform.position = new Vector3(gameGridObjects[j, i].transform.position.x, gameGridObjects[j, i].transform.position.y - 1.5f, 0);
						// gameGridObjects[j, i].col -= 1;
						//gameGridObjects[j, i - 1] = gameGridObjects[j, i];
						//gameGridObjects[j, i] = null;
						if (gameGrid [j, i].cellType == GridCell.type.Bomb || gameGrid [j, i].cellType == GridCell.type.Taunt|| gameGrid [j, i].cellType == GridCell.type.Infected|| gameGrid [j, i].cellType == GridCell.type.Portal) {
							gameGridObjects [j, i].transform.Find ("bombIdle").GetComponent<Animator> ().Play ("bombAttackIdle");
						}
					}
					// gameGrid[j, i - 1] = gameGrid[j, i];
				}
			}
		}
        for (int i = 0; i < 7; i++)
        {
			if (gameGrid[i, 0].cellType == GridCell.type.Bomb || gameGrid[i, 0].cellType == GridCell.type.Taunt || gameGrid[i, 0].cellType == GridCell.type.Infected || gameGrid[i, 0].cellType == GridCell.type.Portal)
            {
                HighScore.SetActive(true);
                if (!endless)
                {
                    PlayerPrefs.SetInt("MaxLevel", levelCounter);
                    endscreen.SetActive(true);
                    pause.SetActive(false);
                    Time.timeScale = 0;
                    Winner.SetActive(false);
                    NextButton.SetActive(false);
                    Shooter.SetActive(false);
                }
                else
                {
                    pause.SetActive(false);
                    Time.timeScale = 0;
                    Winner.SetActive(false);
                    NextButton.SetActive(false);
                    Shooter.SetActive(false);
                    if (HighScore.GetComponent<HighScore>().IsHighScore(levelCounter))
                    {
                        HighScore.transform.Find("Scores").gameObject.SetActive(true);
                    }
                    else
                    {
                        endscreen.SetActive(true);
                    }
                    
                }

            }
            else if(gameGridObjects[i, 0] != null)
	        {
                            Destroy(gameGridObjects[i, 0].gameObject);
                            gameGridObjects[i, 0] = null;
							gameGrid[i, 0].cellType = GridCell.type.Empty;
	        }
        }
	}

	public void EmptyCell(int row, int col)
	{
		gameGridObjects [row, col] = null;
        gameGrid[row, col].cellType = GridCell.type.Empty;
	}

	IEnumerator Transition(int row, int col, float time)
	{
		Vector3 startPos = gameGridObjects [row, col].transform.position;
		float timeSpent = 0;
		while (timeSpent<time)
		{
			gameGridObjects [row, col].transform.position = Vector3.Lerp (gameGridObjects [row, col].transform.position, new Vector3 (startPos.x, startPos.y - 1.5f, 0), (timeSpent/time));
			timeSpent += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
        gameGridObjects[row, col].transform.position = Vector3.Lerp(gameGridObjects[row, col].transform.position, new Vector3(startPos.x, startPos.y - 1.5f, 0), 1);
    }

    void LoadLevel()
    {
		if(testLevel && testLevelAsset != null)
        {
            bombCount = loadLevel.ReadGrid(this, testLevelAsset);
        }
        else
        {
            bombCount = loadLevel.ReadGrid(this, (TextAsset)levelManager[PlayerPrefs.GetInt("LoadLevel")]);
        }
    }

    public void LevelFinished()
    {
        endscreen.SetActive(true);
        pause.SetActive(false);
        Loser.SetActive(false);
        Time.timeScale = 0;
        Shooter.SetActive(false);
		if(PlayerPrefs.GetInt("LoadLevel") == 34)
			{
				NextButton.SetActive(false);
				RetryButton.SetActive (false);
			}
    }

	bool BombCount()
	{
		for (int i = 0; i < 50; i++)
		{
			for (int k = 0; k < 7; k++)
			{
				if (gameGrid[k,i].cellType == GridCell.type.Bomb || gameGrid[k,i].cellType == GridCell.type.Taunt || gameGrid[k,i].cellType == GridCell.type.Infected || gameGrid[k,i].cellType == GridCell.type.Portal)
				{
					return true;
				}
			}
		}
		return false;
	}
}
