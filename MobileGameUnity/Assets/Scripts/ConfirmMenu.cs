﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmMenu : MonoBehaviour {
	public GameObject UpgradeMenu;
	public GameObject SelectMenu;
	ItemsScript Item;

	public void ShowPopUP (ItemsScript item)
	{
		Item = item;
		gameObject.SetActive (true);
		//Debug.Log ("Price of the Item is : " + Item.UpgradePrice);

	}

	public void ConfirmPopUp () 
	{
		SelectMenu.SetActive (false);
		UpgradeMenu.SetActive (true);
		if (GameWallet.HasEnoughCurrency (Item.UpgradePrice)) {
			OnYes ();

		} 
		else 
		{
			ConfirmNo ();
		}
	}

	public void ConfirmNo()
	{
		SelectMenu.SetActive (false);
		UpgradeMenu.SetActive (true);
		if (!GameWallet.HasEnoughCurrency (Item.UpgradePrice)) 
		{
			NoCurrency ();
		}

	}

	public void OnYes()
	{
		GameWallet.AddCurrency (-Item.UpgradePrice);
		ItemSaves.UnlockUpgrade(Item.UpgradeID);
		ItemSaves.SetLastUpgradeChosen (Item.UpgradeID,Item.Category);
		Item.Disable ();

	}

	public void NoCurrency() 
	{
		GameWallet.HasEnoughCurrency (Item.UpgradePrice);
	}
	public void OnNo()
	{
		SelectMenu.SetActive (false);
		UpgradeMenu.SetActive (true);

	}

}
