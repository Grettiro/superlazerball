﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class buttonClicked : MonoBehaviour {

	public GameObject Credits;
	public GameObject Startgame;
	public GameObject Quit;
	public GameObject Upgrade;
	public GameObject Highscore;
	public GameObject GameMode;
	public GameObject StoryMode;
	public GameObject EndlessMode;
	public GameObject StoryComic;
	public GameObject Back;
	public GameObject LoadingScreen;
	public GameObject levelSelect;
	public AudioClip backPressed;
	public AudioClip credit2;
    public AdControl adControl;

	public Animator creditsAnim;

	public Animator gameModeAnim;

	public Animator highscoreAnim;

	public Animator storymodeAnim;

	// Use this for initialization
	void Start () {
		//creditsAnim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CreditsPressed()
	{
		//Credits.SetActive(true);
		creditsAnim.SetTrigger ("On");
		//creditsAnim.SetTrigger ("CreditsOn");
	}

	/*public void GameModePressed(){

		creditsAnim.SetTrigger ("Off");
	}*/

	public void BackIconPressed()
	{
		Back.SetActive (false);
	}

	public void GameModeBackButtonPressed(){
		GameMode.SetActive (false);
		gameModeAnim.SetTrigger ("Off");
	}

	public void HighscoreBackButtonPressed(){
		Highscore.SetActive (false);
		highscoreAnim.SetTrigger ("Off");
	}

	public void MainMenu()
	{
		//Credits.SetActive (false);
		creditsAnim.SetTrigger ("Off");
	}

	public void StartGame(int mode)
	{
        adControl.DestroyBanner();
        Time.timeScale = 1;
		if (mode == 1) 
		{
			PlayerPrefs.SetString ("GameMode", "endless");
			LevelLoadHelper (1);

		} 
		else if (mode == 2) 
		{
			PlayerPrefs.SetString ("GameMode", "story");
			StoryComic.SetActive (true);
			storymodeAnim.SetTrigger ("On");
		}
			
        

	}

    public void LoadLevel(int level)
    {
        PlayerPrefs.SetInt("LoadLevel", level);
        Time.timeScale = 1;
		levelSelect.SetActive (false);
        LevelLoadHelper (1);
        
    }

	public void StartButtonPressed(){

		gameModeAnim.SetTrigger ("On");
	}

	public void GameModeMenu(){

		GameMode.SetActive (true);

		//creditsAnim.SetTrigger ("Off");
		gameModeAnim.SetTrigger ("On");
		//SceneManager.LoadScene (4);
	}

	public void ExitGame()
	{
		Application.Quit ();
	}

	public void UpgradesPressed()
	{
		//SceneManager.LoadScene (2);
		Upgrade.SetActive (true);
	}

	public void HighscoreMenu(){

		Highscore.SetActive (true);
		highscoreAnim.SetTrigger ("On");
	}

	public void HighscorePressed()
	{
		//SceneManager.LoadScene ("Highscore Menu");
		Highscore.SetActive (true);
		highscoreAnim.SetTrigger ("On");
	}

    /*public void GameModeMenu(){
		
		SceneManager.LoadScene (5);
	}

	public void CreditsMenu()
	{
		SceneManager.LoadScene (4);
	}*/

    public void MainMenuScene()
    {
        SceneManager.LoadScene("Main_Menu Scene");
        Time.timeScale = 1;
    }

    public void NextLevel()
    {
		bool coinlevel = false;
		if (PlayerPrefs.GetInt ("LoadLevel") == 34) {
			PlayerPrefs.SetInt ("FinishedLevels", 34);
			SceneManager.LoadScene (0);
			Time.timeScale = 1;
		} 
		else 
		{
			if (PlayerPrefs.GetInt ("FinishedLevels", 0) <= PlayerPrefs.GetInt ("LoadLevel")) {
				PlayerPrefs.SetInt ("FinishedLevels", PlayerPrefs.GetInt ("FinishedLevels") + 1);
			}
			if (PlayerPrefs.GetInt ("LoadLevel") == 8 && PlayerPrefs.GetInt ("CoinLevelFinished") == 1) {
				PlayerPrefs.SetInt ("LoadLevel", PlayerPrefs.GetInt ("LoadLevel") + 2);
			}
			if (PlayerPrefs.GetInt ("LoadLevel") == 9) {
				PlayerPrefs.SetInt ("CoinLevelFinished", 1);
				PlayerPrefs.SetInt ("LoadLevel", PlayerPrefs.GetInt ("LoadLevel") + 1);
				coinlevel = true;
			}
			if (PlayerPrefs.GetInt ("LoadLevel") != 9 && !coinlevel) {
				PlayerPrefs.SetInt ("LoadLevel", PlayerPrefs.GetInt ("LoadLevel") + 1);
			}

			SceneManager.LoadScene (1);
			Time.timeScale = 1;
		}
    }
	public void ResetLevel()
	{
		SceneManager.LoadScene (1);
		Time.timeScale = 1;
	}

	public void LevelLoadHelper(int level)
	{
		LoadingScreen.SetActive (true);
		//loadingAnim.SetTrigger ("On");
		StartCoroutine (LevelLoading (level));
	}
	IEnumerator LevelLoading(int levelNumber)
	{
		yield return new WaitForSeconds (0.05f);
		AsyncOperation async = SceneManager.LoadSceneAsync (levelNumber);
		while (!async.isDone) {
			yield return null;
		}
	}
}
