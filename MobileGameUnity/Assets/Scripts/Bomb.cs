﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : GameGridObject
{
    public int health;
    public int initialHealth;
	public GameObject explosion;
    public AudioClip explodesound;
    public AudioClip bombhitsound;
    public Powerup powerup;
	public bool tauntBomb = false;
    public bool infectedBomb = false;
	public bool portalBomb = false;
	public bool immune = false;
	SoundManager soundManager;
	AudioSource audioSource;
    private GameObject bombText;
	public TextMesh textUpdate;

    private void Awake() {
        textUpdate = GetComponentInChildren<TextMesh>();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
		audioSource = GetComponent<AudioSource> ();
		if (gameObject.tag == "Taunt") {
			tauntBomb = true;
		}
		if (gameObject.tag == "Infected") {
			infectedBomb = true;
		}
		if (gameObject.tag == "Portal") {
			portalBomb = true;
		}
    }
		
    // Update is called once per frame
    void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
			
			if (!immune) {
				if (!portalBomb) {
					audioSource.PlayOneShot (bombhitsound, soundManager.volume);
					HealthManager (collision.gameObject);
				}
				if (portalBomb) {
					audioSource.PlayOneShot (bombhitsound, soundManager.volume);
					PortalBomb (collision);
					HealthManager (collision.gameObject);
				}
			} else if (immune && portalBomb) {
				PortalBomb (collision);
			}
        }	
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Piercing")
        {
            if(!immune)
            {
                HealthManager(collision.transform.parent.gameObject);
            }
            
        }
    }

    private void HealthManager(GameObject ball)
    {
            if (ball.GetComponent<BallScript>().doubleDamage > 0)
            {
                health -= ball.GetComponent<BallScript>().doubleDamage * 2;
				textUpdate.text = health.ToString();
                if (health <= 0)
                {
                    health = 0;
                }
            }
            else if(ball.GetComponent<BallScript>().instaKill)
            {
                GameObject.Find("Shooter").GetComponent<ShootBall>().returnedAmmo += 1;
				health = 0;
                Destroy(ball);
            }
            else
            {
                health--;
				textUpdate.text = health.ToString();
                if (health <= 0)
                {
                    health = 0;
                }
            }
            if (health != 0)
            {
                if (transform.Find("ParticleSystem").gameObject.activeInHierarchy)
                {
                    transform.Find("ParticleSystem").gameObject.SetActive(false);
                }
                if (transform.Find("ParticleSystem2").gameObject.activeInHierarchy)
                {
                    transform.Find("ParticleSystem2").gameObject.SetActive(false);
                }
                transform.Find("ParticleSystem").gameObject.SetActive(true);
                transform.Find("ParticleSystem2").gameObject.SetActive(true);

            }
            if (health == 0)
            {
                transform.Find("ParticleSystem").gameObject.SetActive(false);
                transform.Find("ParticleSystem2").gameObject.SetActive(false);
                DestroyBomb();
            }
    }

	private void PortalBomb(Collision2D collision)
	{
		bool samePortal = true;
		bool teleported = false;
		bool alternatePortal = true;
		List<GameObject> portals = new List<GameObject>();
		int numPortals = 0;
		for (int i = 0; i < 10; i++) {
			for (int k = 0; k < 7; k++) {
				if (grid.gameGrid [k, i].cellType == GridCell.type.Portal) {
					portals.Add(grid.gameGridObjects [k, i].gameObject);
					numPortals++;
				}
			}
		}
		if (numPortals>1) {
			int randomLength = Random.Range (0, numPortals);
			while (samePortal) {
				if (portals [randomLength].gameObject != gameObject) {
					samePortal = false;
				} else {
					randomLength = Random.Range (0, numPortals);
				}
			}
			if (!teleported) {
				int rowContainer = portals[randomLength].GetComponent<Bomb>().row;
				int colContainer = portals[randomLength].GetComponent<Bomb> ().col;
				Rigidbody2D rbContainer = collision.contacts [0].collider.gameObject.GetComponent<Rigidbody2D> ();
				TrailRenderer[] trails = collision.contacts [0].collider.gameObject.transform.Find ("Trails").gameObject.GetComponentsInChildren<TrailRenderer>();
				float top = Mathf.Abs(collision.contacts [0].point.y - 0.65f - transform.position.y);
				float bottom = Mathf.Abs(collision.contacts [0].point.y + 0.65f - transform.position.y);
				float right = Mathf.Abs(collision.contacts [0].point.x - 0.65f - transform.position.x);
				float left = Mathf.Abs(collision.contacts [0].point.x + 0.65f - transform.position.x);

				if (collision.contacts [0].point.y + 0.65 < transform.position.y && bottom < right && bottom < left) { // then it's hitting the bottom
					if (colContainer < 9) {
						if (grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Ammo) {
							collision.contacts [0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y + 0.85f, portals [randomLength].transform.position.z);
							rbContainer.velocity = new Vector2 (rbContainer.velocity.x, -rbContainer.velocity.y);
							alternatePortal = false;
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					} 
					if (colContainer >= 1 && alternatePortal) {
							if (grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Ammo) {
								collision.contacts [0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y - 0.85f, portals [randomLength].transform.position.z);
								alternatePortal = false;
								foreach (TrailRenderer trail in trails) {
									trail.Clear ();
								}
							}
					}
					if (rowContainer < 6 && alternatePortal) {
							if(grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Ammo  && rowContainer < 6)
							{
								collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x  + 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
								alternatePortal = false;
								if (rbContainer.velocity.x <= 0) {
									rbContainer.velocity = new Vector2 (-rbContainer.velocity.x, rbContainer.velocity.y);
								}
								foreach (TrailRenderer trail in trails) {
									trail.Clear ();
								}
							}
						}
					if (rowContainer >= 1) {
						if(grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Powerup  || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Ammo && rowContainer >= 1)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x - 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
							if (rbContainer.velocity.x >= 0) {
								rbContainer.velocity = new Vector2 (-rbContainer.velocity.x, rbContainer.velocity.y);
							}
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
						}
				} 
				else if (collision.contacts [0].point.y - 0.65 > transform.position.y && top < right + 0.1f && top < left + 0.1f) // Then it's hitting the top
				{
					if (colContainer >= 1) {
						if (grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Ammo  && colContainer >= 1) {
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y - 0.85f, portals [randomLength].transform.position.z);
							alternatePortal = false;
							rbContainer.velocity = new Vector2 (rbContainer.velocity.x, -rbContainer.velocity.y);
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (colContainer < 9 && alternatePortal) {
						if(grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Ammo && colContainer <= 9)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y + 0.85f, portals [randomLength].transform.position.z);
							alternatePortal = false;
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (rowContainer < 6 && alternatePortal) {
						if(grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Ammo && rowContainer <= 6)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x  + 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
							alternatePortal = false;
							if (rbContainer.velocity.x <= 0) {
								rbContainer.velocity = new Vector2 (-rbContainer.velocity.x, -rbContainer.velocity.y);
							}
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (rowContainer >= 1 && alternatePortal) {
						if(grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Ammo && rowContainer >= 1)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x - 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
							if (rbContainer.velocity.x <= 0) {
								rbContainer.velocity = new Vector2 (-rbContainer.velocity.x, -rbContainer.velocity.y);
							}
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
				} 
				else if (collision.contacts [0].point.x + 0.65 < transform.position.x) //Then it's hitting the left side
				{
					if (rowContainer < 6) {
						if (grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Ammo && rowContainer < 6) {
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x + 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
							alternatePortal = false;
							rbContainer.velocity = new Vector2 (-rbContainer.velocity.x, rbContainer.velocity.y);
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (rowContainer >= 1 && alternatePortal) {
						if(grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Ammo && rowContainer >= 1)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x - 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
							alternatePortal = false;
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (colContainer < 9 && alternatePortal) {
						if(grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Ammo && colContainer < 6)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y  + 0.85f, portals [randomLength].transform.position.z);
							alternatePortal = false;
							if (rbContainer.velocity.y <= 0) {
								rbContainer.velocity = new Vector2 (rbContainer.velocity.x, -rbContainer.velocity.y);
							}
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (colContainer >= 1 && alternatePortal) {
						if(grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Ammo && colContainer >= 1)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y - 0.85f, portals [randomLength].transform.position.z);
							if (rbContainer.velocity.y >= 0) {
								rbContainer.velocity = new Vector2 (rbContainer.velocity.x, -rbContainer.velocity.y);
							}
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
				} 
				else if (collision.contacts [0].point.x - 0.65 > transform.position.x) //Then it's hitting the right side
				{
					if (rowContainer >= 1) {
						if (grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer - 1, colContainer].cellType == GridCell.type.Ammo && rowContainer >= 1) {
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x - 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
							alternatePortal = false;
							rbContainer.velocity = new Vector2 (-rbContainer.velocity.x, rbContainer.velocity.y);
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (rowContainer < 6 && alternatePortal) {
						if(grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer + 1, colContainer].cellType == GridCell.type.Ammo && rowContainer < 6)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x + 0.85f, portals [randomLength].transform.position.y, portals [randomLength].transform.position.z);
							alternatePortal = false;
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (colContainer < 9) {
						if(grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer + 1].cellType == GridCell.type.Ammo && colContainer < 6)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y  + 0.85f, portals [randomLength].transform.position.z);
							alternatePortal = false;
							if (rbContainer.velocity.y <= 0) {
								rbContainer.velocity = new Vector2 (rbContainer.velocity.x, -rbContainer.velocity.y);
							}
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
					if (colContainer >= 1 && alternatePortal) {
						if(grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Empty || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Currency || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Powerup || grid.gameGrid [rowContainer, colContainer - 1].cellType == GridCell.type.Ammo && colContainer >= 1)
						{
							collision.contacts[0].collider.transform.position = new Vector3 (portals [randomLength].transform.position.x, portals [randomLength].transform.position.y - 0.85f, portals [randomLength].transform.position.z);
							if (rbContainer.velocity.y >= 0) {
								rbContainer.velocity = new Vector2 (rbContainer.velocity.x, -rbContainer.velocity.y);
							}
							foreach (TrailRenderer trail in trails) {
								trail.Clear ();
							}
						}
					}
				}
			}
			else
			{ 
				if (collision.transform.position.x > 4.7f) {
					collision.transform.position= new Vector3(4.65f,collision.transform.position.y, collision.transform.position.z);
				}
				if (collision.transform.position.x < -4.7) {
					collision.transform.position= new Vector3(-4.7f,collision.transform.position.y, collision.transform.position.z);
				}
				if (collision.transform.position.y > 10.5) {
					collision.transform.position= new Vector3(collision.transform.position.x,10.5f, collision.transform.position.z);
				}
			}
		}
	}
    private void DestroyBomb()
    {
        grid.EmptyCell(row, col);
		if (tauntBomb) {
            transform.Find("Glow").gameObject.SetActive(false);
            bool extraTaunt = false;
			for (int i = 0; i < 10; i++) {
				for (int k = 0; k < 7; k++) {
					if (grid.gameGrid [k, i].cellType == GridCell.type.Taunt) {
						extraTaunt = true;
					}
				}
			}
			if (!extraTaunt) {
				GameObject[] bombs;
				bombs = GameObject.FindGameObjectsWithTag ("Bomb");
				foreach (GameObject bomb in bombs) {
					bomb.GetComponent<Bomb> ().immune = false;
				}
				GameObject[] infecteds;
				infecteds = GameObject.FindGameObjectsWithTag ("Infected");
				foreach(GameObject infect in infecteds)
				{
					infect.GetComponent<Bomb> ().immune = false;
				}
				GameObject[] portals;
				portals = GameObject.FindGameObjectsWithTag ("Portal");
				foreach(GameObject portal in portals)
				{
					portal.GetComponent<Bomb> ().immune = false;
				}
			}
		}
        if(infectedBomb)
        {
            transform.Find("Glow").gameObject.SetActive(false);
            Infect();
        }
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        transform.Find("BombExplosion").gameObject.SetActive(true);
        transform.Find("Forcefield").GetComponent<SpriteRenderer>().enabled = false;
        transform.Find("bombIdle").gameObject.SetActive(false);
        transform.Find("Glow").gameObject.SetActive(false);
        audioSource.pitch = Random.Range(0.8f, 1.2f);
		audioSource.PlayOneShot(explodesound, soundManager.volume);
		Destroy(gameObject, explodesound.length);
		if (grid.endless) {
			int powerupSpawn = Random.Range (1, 100);
				if (powerupSpawn < 6) 
				{
					switch (Random.Range (1, 6)) {
					case 1:
						grid.SetCell (row, col, new GridCell (0, 0, GridCell.powerupType.DoubleDamage, GridCell.type.Powerup));
						break;
					case 2:
						grid.SetCell (row, col, new GridCell (0, 0, GridCell.powerupType.InstantBomb, GridCell.type.Powerup));
						break;
					case 3:
						grid.SetCell (row, col, new GridCell (0, 0, GridCell.powerupType.Piercing, GridCell.type.Powerup));
						break;
					case 4:
						grid.SetCell (row, col, new GridCell (0, 0, GridCell.powerupType.Mirror, GridCell.type.Powerup));
						break;
					case 5:
						grid.SetCell (row, col, new GridCell (0, 0, GridCell.powerupType.ShootNMove, GridCell.type.Powerup));
						break;
					default:
						break;
					}
				}
		}
    }
    void Infect()
	{
		if (row != 6 && col != 0) {
			if (grid.gameGrid [row + 1, col - 1].cellType == GridCell.type.Bomb || grid.gameGrid [row + 1, col - 1].cellType == GridCell.type.Infected|| grid.gameGrid [row + 1, col - 1].cellType == GridCell.type.Taunt|| grid.gameGrid [row + 1, col - 1].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row + 1, col - 1].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row + 1, col - 1].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row + 1, col - 1].gameObject.GetComponent<Bomb> ().health.ToString();
                grid.gameGridObjects[row + 1, col - 1].transform.Find("Glow").gameObject.SetActive(true);

            }
		}
		if (col != 0) {
			if (grid.gameGrid [row, col - 1].cellType == GridCell.type.Bomb || grid.gameGrid [row, col - 1].cellType == GridCell.type.Infected|| grid.gameGrid [row, col - 1].cellType == GridCell.type.Taunt|| grid.gameGrid [row, col - 1].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row, col - 1].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row, col - 1].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row, col - 1].gameObject.GetComponent<Bomb> ().health.ToString();
                if (grid.gameGridObjects[row, col - 1].transform.Find("Glow").gameObject.activeInHierarchy == true)
                {
                    grid.gameGridObjects[row, col - 1].transform.Find("Glow").gameObject.SetActive(false);
                }
                grid.gameGridObjects[row, col - 1].transform.Find("Glow").gameObject.SetActive(true);
            }
		}
		if (row != 0 && col != 0) {
			if (grid.gameGrid [row - 1, col - 1].cellType == GridCell.type.Bomb || grid.gameGrid [row - 1, col - 1].cellType == GridCell.type.Infected|| grid.gameGrid [row - 1, col - 1].cellType == GridCell.type.Taunt|| grid.gameGrid [row - 1, col - 1].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row - 1, col - 1].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row - 1, col - 1].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row - 1, col - 1].gameObject.GetComponent<Bomb> ().health.ToString();
                if (grid.gameGridObjects[row - 1, col - 1].transform.Find("Glow").gameObject.activeInHierarchy == true)
                {
                    grid.gameGridObjects[row - 1, col - 1].transform.Find("Glow").gameObject.SetActive(false);
                }
                grid.gameGridObjects[row - 1, col - 1].transform.Find("Glow").gameObject.SetActive(true);
            }
		}
		if (row != 6 && col != 49) {
			if (grid.gameGrid [row + 1, col + 1].cellType == GridCell.type.Bomb || grid.gameGrid [row + 1, col + 1].cellType == GridCell.type.Infected|| grid.gameGrid [row + 1, col + 1].cellType == GridCell.type.Taunt|| grid.gameGrid [row + 1, col + 1].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row + 1, col + 1].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row + 1, col + 1].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row + 1, col + 1].gameObject.GetComponent<Bomb> ().health.ToString();
                if (grid.gameGridObjects[row + 1, col + 1].transform.Find("Glow").gameObject.activeInHierarchy == true)
                {
                    grid.gameGridObjects[row + 1, col + 1].transform.Find("Glow").gameObject.SetActive(false);
                }
                grid.gameGridObjects[row + 1, col + 1].transform.Find("Glow").gameObject.SetActive(true);
            }
		}
		if (row != 0) {
			if (grid.gameGrid [row - 1, col].cellType == GridCell.type.Bomb || grid.gameGrid [row - 1, col].cellType == GridCell.type.Infected|| grid.gameGrid [row - 1, col].cellType == GridCell.type.Taunt|| grid.gameGrid [row - 1, col].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row - 1, col].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row - 1, col].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row - 1, col].gameObject.GetComponent<Bomb> ().health.ToString();
                if (grid.gameGridObjects[row - 1, col].transform.Find("Glow").gameObject.activeInHierarchy == true)
                {
                    grid.gameGridObjects[row - 1, col].transform.Find("Glow").gameObject.SetActive(false);
                }
                grid.gameGridObjects[row - 1, col].transform.Find("Glow").gameObject.SetActive(true);
            }
		}
		if (row != 6) {
			if (grid.gameGrid [row + 1, col].cellType == GridCell.type.Bomb || grid.gameGrid [row +1, col].cellType == GridCell.type.Infected|| grid.gameGrid [row +1, col].cellType == GridCell.type.Taunt|| grid.gameGrid [row +1, col].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row + 1, col].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row + 1, col].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row + 1, col].gameObject.GetComponent<Bomb> ().health.ToString();
                if (grid.gameGridObjects[row + 1, col].transform.Find("Glow").gameObject.activeInHierarchy == true)
                {
                    grid.gameGridObjects[row + 1, col].transform.Find("Glow").gameObject.SetActive(false);
                }
                grid.gameGridObjects[row + 1, col].transform.Find("Glow").gameObject.SetActive(true);
            }
		}
		if (row != 0 && col != 49) {
			if (grid.gameGrid [row - 1, col + 1].cellType == GridCell.type.Bomb || grid.gameGrid [row - 1, col + 1].cellType == GridCell.type.Infected|| grid.gameGrid [row - 1, col + 1].cellType == GridCell.type.Taunt|| grid.gameGrid [row - 1, col + 1].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row - 1, col + 1].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row - 1, col + 1].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row - 1, col + 1].gameObject.GetComponent<Bomb> ().health.ToString();
                if (grid.gameGridObjects[row - 1, col + 1].transform.Find("Glow").gameObject.activeInHierarchy == true)
                {
                    grid.gameGridObjects[row - 1, col + 1].transform.Find("Glow").gameObject.SetActive(false);
                }
                grid.gameGridObjects[row - 1, col + 1].transform.Find("Glow").gameObject.SetActive(true);
            }
		}
		if (col != 49) {
			if (grid.gameGrid [row, col + 1].cellType == GridCell.type.Bomb || grid.gameGrid [row, col + 1].cellType == GridCell.type.Infected|| grid.gameGrid [row, col + 1].cellType == GridCell.type.Taunt|| grid.gameGrid [row, col + 1].cellType == GridCell.type.Portal) {
				grid.gameGridObjects [row, col + 1].gameObject.GetComponent<Bomb> ().health += Mathf.CeilToInt((float)initialHealth / 2);
				grid.gameGridObjects [row, col + 1].gameObject.GetComponent<Bomb> ().textUpdate.text = grid.gameGridObjects [row, col + 1].gameObject.GetComponent<Bomb> ().health.ToString();
                if (grid.gameGridObjects[row, col + 1].transform.Find("Glow").gameObject.activeInHierarchy == true)
                {
                    grid.gameGridObjects[row, col + 1].transform.Find("Glow").gameObject.SetActive(false);
                }
                grid.gameGridObjects[row, col + 1].transform.Find("Glow").gameObject.SetActive(true);
            }
		}
    }
}
