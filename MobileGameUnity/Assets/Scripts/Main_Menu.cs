﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Main_Menu : MonoBehaviour {
	public Button PlayGame;
	public Button UpgradeMenu;
	public Button QuitButton;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	public void Gameplay () 
	{
		PlayGame.enabled = true;
		SceneManager.LoadScene ("Ballllz");
	}

	public void MenuUpgrade()
	{
		UpgradeMenu.enabled = true;
		SceneManager.LoadScene ("Upgrade Menu");
	}

	public void ExitGame()
	{
		QuitButton.enabled = true;
		Application.Quit ();
	}
}
