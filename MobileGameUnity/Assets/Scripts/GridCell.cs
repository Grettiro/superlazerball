﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class GridCell{
		public enum powerupType{Empty, Piercing, DoubleDamage, ShootNMove, Mirror, InstantBomb};
		public enum type{Empty, Bomb, Powerup, Currency, Ammo, Taunt, Infected, Portal};
		public int bombValue;
		public int currency;
		public powerupType powerup;
		public type cellType;


		public GridCell(int value, int curr, powerupType power, type type)
		{
			bombValue = value;
			currency = curr;
			powerup = power;
			cellType = type;
		}

}
