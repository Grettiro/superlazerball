﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class GameWallet 
{

	static public int GetTotalCurrency()
	{

		return PlayerPrefs.GetInt ("TotalCurrency");
	}

	static public void ResetCurrency()
	{
		PlayerPrefs.SetInt ("TotalCurrency",0);
	}


	// Update is called once per frame

	static public void AddCurrency(int currencytoadd)
	{
		int TotalCurrency = GetTotalCurrency();
		TotalCurrency += currencytoadd;

		PlayerPrefs.SetInt ("TotalCurrency", TotalCurrency);
	}

	static public bool HasEnoughCurrency(int price)
	{
		int TotalCur = GetTotalCurrency ();

		if (price <= TotalCur) 
		{
			
			return true;
		} 
		else 
		{
			Debug.Log ("Not Enough Money");
			return false;
		}

	}

	/*void Update()
	{
		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			Debug.Log ("Money is" + GetTotalCurrency());
		}

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			AddCurrency (1);
			Debug.Log ("Current Stock : " + GetTotalCurrency());
		}


		if (Input.GetKeyDown (KeyCode.Return)) 
		{
			Debug.Log (HasEnoughCurrency(100));
		} 
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			Debug.Log (HasEnoughCurrency(10));
		}

	}*/

}