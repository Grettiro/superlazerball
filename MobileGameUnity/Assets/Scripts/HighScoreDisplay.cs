﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreDisplay : MonoBehaviour {
    public Text highScore1;
    public Text name1;
    public Text highScore2;
    public Text name2;
    public Text highScore3;
    public Text name3;
    public Text highScore4;
    public Text name4;
    public Text highScore5;
    public Text name5;
    // Use this for initialization
    void Start () {
        highScore1.text = PlayerPrefs.GetInt("Score1").ToString();
        name1.text = PlayerPrefs.GetString("Name1");
        highScore2.text = PlayerPrefs.GetInt("Score2").ToString();
        name2.text = PlayerPrefs.GetString("Name2");
        highScore3.text = PlayerPrefs.GetInt("Score3").ToString();
        name3.text = PlayerPrefs.GetString("Name3");
        highScore4.text = PlayerPrefs.GetInt("Score4").ToString();
        name4.text = PlayerPrefs.GetString("Name4");
        highScore5.text = PlayerPrefs.GetInt("Score5").ToString();
        name5.text = PlayerPrefs.GetString("Name5");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
