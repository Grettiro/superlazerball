﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class ItemSaves
{
	

	static public void UnlockUpgrade(int UpgradID)
	{
		PlayerPrefs.SetInt( UpgradeNames[UpgradID] , 1);

	}
	static public void LockUpgrade (int UpgradID)
	{
		PlayerPrefs.SetInt( UpgradeNames[UpgradID] , 0);

	}
		


	static public bool IsUpgradeUnlocked(int UpID)
	{
		if (PlayerPrefs.GetInt (UpgradeNames[UpID],0) == 1) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}

	static string []UpgradeNames = {"TrajectoryGuide1","TrajectoryGuide2","RedBall","BlueBall","GreenBall","PinkBall","PurpleBall","YellowBall","OrangeBall","RedGun","BlueGun","GreenGun","PinkGun","PurpleGun","YellowGun","OrangeGun","DefaultBall","DefaultGun"};


	// This function is to  Set upgrade chosen.
	static public void SetLastUpgradeChosen(int UpgradID, UpgradeCategory Category)
	{
		PlayerPrefs.SetInt (Category.ToString(), UpgradID);

	}

	// This function is to Get upgrade chosen.
	static public int GetLastUpgradeChosen(UpgradeCategory Category)
	{
        //Debug.Log("Upgrade Name is " + Category.ToString());
        return PlayerPrefs.GetInt (Category.ToString (), -1);		 
	}

	static public string GetLastUpgradeChosenName(UpgradeCategory Category)
	{
		string name = "";
		if (GetLastUpgradeChosen (Category) != -1) 
		{
			name = UpgradeNames[GetLastUpgradeChosen (Category)];
		} 
		return name;
	}
	public enum UpgradeCategory {Traj,BallColor,GunColor};

		

}

