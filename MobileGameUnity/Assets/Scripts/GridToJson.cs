﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class GridToJson{
    public GridCell[] gridArray = new GridCell[350];
	public void WriteGrid(GridCell[] cell)
	{
        int cellCount = 0;
		string jsonTest;
		for (int i = 0; i < cell.Length; i++) {
			for (int k = 0; k < 7; k++) {
                gridArray[cellCount] = cell[cellCount];
                cellCount++;
			}
		}
        jsonTest = JsonHelper.ToJson(gridArray, true);
        File.AppendAllText("Resources/Levels/Grid.json", jsonTest); //Change path to Application.dataPath before building for release
    }

	public void WriteGrid(GridCell[] cell, string path)
	{
		/*int cellCount = 0;
		string jsonTest;
		for (int i = 0; i < cell.Length; i++) {
			for (int k = 0; k < 7; k++) {
				gridArray[cellCount] = cell[cellCount];
				cellCount++;
			}
		}*/
		string jsonTest = JsonHelper.ToJson(cell, true);
		//File.Create(path);
		File.WriteAllText(path, jsonTest); //Change path to Application.dataPath before building for release
		#if UNITY_EDITOR
		UnityEditor.AssetDatabase.Refresh();
		#endif
	}
}
