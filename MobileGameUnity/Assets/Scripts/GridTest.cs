﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTest : MonoBehaviour {
    public Grid grid;
    public GridToJson jsonWriteTest;
    private int round = 1;
    private int currencyRound = 1;
    private void Start()
    {
           for (int i = 0; i < 50; i++)
           {
            if (i > 7) { 
                List<int> spawnPoints = new List<int>();
                for (int l = 0; l < 7; l++)
                {
                    spawnPoints.Add(l);
                }
                for (int l = 0; l < spawnPoints.Count; l++)
                {
                    int temp = spawnPoints[l];
                    int randomIndex = Random.Range(l, spawnPoints.Count);
                    spawnPoints[l] = spawnPoints[randomIndex];
                    spawnPoints[randomIndex] = temp;
                }

                int spawnAmount = 0;
                int rand = Random.Range(0, 16);
                if (rand < 4)
                {
                    if (currencyRound == 3)
                    {
                        spawnAmount = 3;
                    }
                    else
                    {
                        spawnAmount = 2;
                    }

                }
                else if (rand < 7)
                {
                    spawnAmount = 3;
                }
                else if (rand < 10)
                {
                    spawnAmount = 4;
                }
                else if (rand < 12)
                {
                    spawnAmount = 5;
                }
                else if (rand < 14)
                {
                    spawnAmount = 6;
                }
                else if (rand <= 15)
                {
                    spawnAmount = 7;
                }
                int[] spawnArray;
                spawnArray = new int[spawnAmount];

                for (int j = 0; j < 7; j++)
                   {
                    bool spawned = false;
                    for (int k = 0; k < spawnArray.Length; k++)
                    {
                        if (j == spawnPoints[k])
                        {
                            spawned = true;
                            if (k == 0)
                            {
                                grid.SetCell(spawnPoints[k], i, new GridCell(0, 0, GridCell.powerupType.Empty, GridCell.type.Ammo));
                            }
                            else if (currencyRound == 3)
                            {
                                currencyRound = 0;
                                grid.SetCell(spawnPoints[k], i, new GridCell(0, 1, GridCell.powerupType.Empty, GridCell.type.Currency));
                            }
                            else
                            {
                                grid.SetCell(spawnPoints[k], i, new GridCell(round, 0, GridCell.powerupType.Empty, GridCell.type.Bomb));
                            }
                        }
                    }
                    if (!spawned)
                    {
                        grid.SetCell(j, i, new GridCell(0, 0, GridCell.powerupType.Empty, GridCell.type.Empty));
                    }
                }
                round++;
                currencyRound++;
            }
            else
            {
                for (int j = 0; j < 7; j++)
                {
                    grid.SetCell(j, i, new GridCell(0, 0, GridCell.powerupType.Empty, GridCell.type.Empty));
                }
            }
        }
       // jsonWriteTest.WriteGrid(grid);
    }

}
