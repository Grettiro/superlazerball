﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsScript : MonoBehaviour {
	public int UpgradePrice;
	public GameObject SureMenu;
	public int UpgradeID;
	public Toggle UpgradeToggle;
	public ConfirmMenu ConfMenu;
	public ItemSaves.UpgradeCategory Category;
	Button ResetButton;
	public Image CoinImage;
	public Text Price;

	void Start () 
	{
		if (ItemSaves.IsUpgradeUnlocked (UpgradeID)) 
		{
			Disable ();
		}

		if (ItemSaves.GetLastUpgradeChosen (Category) == UpgradeID) 
		{
			UpgradeToggle.isOn = true;
		}
		else 
		{
			UpgradeToggle.isOn = false;
		}
	}
	
	public void OnSelect () 
	{
		//To check if this is TrajectoryGuide2
		if (UpgradeID ==1) 
		{
			//To check if TrajectorGuide1 is Unlocked.
			if (ItemSaves.IsUpgradeUnlocked (0)) 
			{
				PurchaseItems ();
			} 
		}
		//This is to check every other upgrade.
		else 
		{
			PurchaseItems ();
					
		}
	}
	public void Reset()
	{
		ItemSaves.LockUpgrade (UpgradeID);
		Enable ();
	}

	public void Disable()
	{
		CoinImage.enabled = false;
		Price.enabled = false;


	}

	public void Enable()
	{
		CoinImage.enabled = true;
		Price.enabled = true;


	}

	public void PurchaseItems()
	{
		if (ItemSaves.IsUpgradeUnlocked (UpgradeID)) 
		{
			ItemSaves.SetLastUpgradeChosen (UpgradeID,Category);
			SureMenu.SetActive (false);
		} 
		else 
		{
			SureMenu.SetActive (true);
			ConfMenu.ShowPopUP (this);
		}
	}

	}
