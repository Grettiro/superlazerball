﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBall : MonoBehaviour {

	GameObject shooter;
	// Use this for initialization
	void Start () {

		shooter = GameObject.Find ("Shooter");
		if (shooter == null) {
			
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Ball") {
			if (shooter != null) {
				shooter.GetComponent<ShootBall> ().returnedAmmo += 1;
			}
			//GameObject.Find ("Shooter").GetComponent<ShootBall> ().returnedAmmo += 1;
			Destroy (collider.gameObject);
		}
	}
}
