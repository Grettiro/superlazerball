﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialColorChange : MonoBehaviour {

	public GameObject cube;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	/*void Update () {

		if (Input.GetKeyDown (KeyCode.R)) {
			SetColorRed();
		}

		if (Input.GetKeyDown (KeyCode.B)) {
			SetColorBlue();
		}

		if (Input.GetKeyDown (KeyCode.G)) {
			SetColorGreen();
		}

		if (Input.GetKeyDown (KeyCode.M)) {
			SetColorMagenta();
		}

		if (Input.GetKeyDown (KeyCode.Y)) {
			SetColorYellow();
		}

		if (Input.GetKeyDown (KeyCode.P)) {
			SetColorPink();
		}

		if (Input.GetKeyDown (KeyCode.O)) {
			SetColorOrange();
		}
			
		
	}*/

	public void SetStartColor(Color color)
	{
		cube.GetComponent<Renderer> ().material.color = color;

	}

	public void SetColorRed()
	{
		SetStartColor (new Color32(195, 30, 30, 155));

	}

	public void SetColorBlue()
	{
		SetStartColor(new Color32(0, 150, 255, 255));

	}

	public void SetColorGreen()
	{
		SetStartColor(new Color32(0, 225, 100, 255));

	}

	public void SetColorMagenta()
	{
		SetStartColor(new Color32(120, 10, 235, 255));

	}

	public void SetColorYellow()
	{
		SetStartColor(new Color32(255, 216, 0, 255));

	}

	public void SetColorPink()
	{
		SetStartColor(new Color32(255, 0, 100, 255));

	}

	public void SetColorOrange()
	{
		SetStartColor(new Color32(255, 69, 0, 255));

	}
}
