﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleColorChange: MonoBehaviour {

	public ParticleSystem particle;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		/*
		if (Input.GetKeyDown (KeyCode.R)) {
			SetColorRed ();
		}

		if (Input.GetKeyDown (KeyCode.B)) {
			SetColorBlue();
		}

		if (Input.GetKeyDown (KeyCode.G)) {
			SetColorGreen();
		}

		if (Input.GetKeyDown (KeyCode.M)) {
			SetColorMagenta();
		}

		if (Input.GetKeyDown (KeyCode.Y)) {
			SetColorYellow();
		}

		if (Input.GetKeyDown (KeyCode.P)) {
			SetColorPink();
		}

		if (Input.GetKeyDown (KeyCode.O)) {
			SetColorOrange();
		}
		*/	
	}

	public void SetStartColor(Color color)
	{
		particle.startColor = color;

	}

	public void SetColorRed()
	{
		SetStartColor (new Color32(255, 95, 0, 255));
		
	}

	public void SetColorBlue()
	{
		SetStartColor(Color.blue);

	}

	public void SetColorGreen()
	{
		SetStartColor(new Color32(62, 255, 62, 255));

	}

	public void SetColorMagenta()
	{
		SetStartColor(Color.magenta);

	}

	public void SetColorYellow()
	{
		SetStartColor(new Color32(255, 195, 0, 255));

	}

	public void SetColorPink()
	{
		SetStartColor(new Color32(255, 211, 237, 255));

	}

	public void SetColorOrange()
	{
		SetStartColor(new Color32(144, 63, 0, 255));
	}
	public void SetColorDefault()
	{
		SetStartColor(new Color32(144, 63, 0, 255));
	}

}
