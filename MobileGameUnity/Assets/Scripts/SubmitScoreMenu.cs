﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SubmitScoreMenu : MonoBehaviour {

	public GameObject SubmitScore;
	public Text HighScoreValue;
	public InputField mainInputField;
    public Grid grid;
    public HighScore highScore;
    //public string playerName;

    // Use this for initialization
    void Start () {
        HighScoreValue.text = grid.levelCounter.ToString();
    }

	// Update is called once per frame
	void Update () {
		
	}

	public void SubmitPressed(){
		SubmitScore.SetActive (false);
        highScore.CheckHighscore(int.Parse(HighScoreValue.text), mainInputField.text);
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }
}
