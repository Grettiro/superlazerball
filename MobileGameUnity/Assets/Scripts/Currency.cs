﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Currency : GameGridObject
{
    public AudioClip currencysound;
	public int m_Value = 1;
    AudioSource audioSource;
	SoundManager soundManager;

    private void Awake() {

        audioSource = GetComponent<AudioSource>();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
    }

    void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Ball") {
			grid.EmptyCell (row, col);
            audioSource.pitch = Random.Range(1f, 1f);
			audioSource.PlayOneShot(currencysound, soundManager.volume);
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            transform.Find("Currency2").gameObject.SetActive(false);
            transform.Find("Particle System").gameObject.SetActive(false);
			GameWallet.AddCurrency (m_Value);
            Destroy (gameObject, currencysound.length);
        }
	}
}
