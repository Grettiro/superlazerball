﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {
	public GameObject[] levelList;
	// Use this for initialization
	void Start () {
		levelList = GameObject.FindGameObjectsWithTag ("LevelSelect");
		for (int i = 0; i < levelList.Length; i++) {
			levelList [i].gameObject.GetComponent<Button> ().interactable = false;
		}
		for (int k = 0; k < PlayerPrefs.GetInt("FinishedLevels",0) + 1; k++) {
			if (k != 9) {
				levelList [k].GetComponent<Button> ().interactable = true;
			}
			if (k == 9 && PlayerPrefs.GetInt ("CoinLevelFinished") == 0) {
				levelList [k].GetComponent<Button> ().interactable = true;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
