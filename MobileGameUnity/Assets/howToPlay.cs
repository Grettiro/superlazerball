﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class howToPlay : MonoBehaviour {

    private float timepassed;
	public Grid grid;
	// Use this for initialization
	void Awake () {
		if (!grid.endless) {
			if (PlayerPrefs.GetInt ("FinishedLevels") != 0) {
				gameObject.SetActive(false);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            gameObject.SetActive(false);
        }
        else
        {
            if(timepassed >= 15f)
            {
                gameObject.SetActive(false);
            }
        }
        timepassed += Time.deltaTime;
	}
}
