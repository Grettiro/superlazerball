﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

	public AudioSource music;
	public AudioSource sounds;
	public static SoundManager instance = null;
	public bool musicToggle;
	public bool soundsToggle;

	public float volume;

	// Use this for initialization
	void Awake()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			DontDestroyOnLoad (gameObject);
		}
		volume = 1;
	}

	public void MuteSound()
	{
		if (volume == 1) {
			volume = 0;
		} else if (volume == 0) {
			volume = 1;
		}
	}

	public void MuteMusic()
	{
		if (music.volume == 1) {
			music.volume = 0;
		} else if (music.volume == 0) {
			music.volume = 1;
		}
	}
	
	// Update is called once per frame
}
