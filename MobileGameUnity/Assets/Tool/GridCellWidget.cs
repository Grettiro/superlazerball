﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GridCellWidget : MonoBehaviour {
	public GridCell Cell;
	public int Row;
	public int Column;
	public Text WidgetHealth;
	public Text WidgetCellType;
	public Text WidgetPowerUp;
	public Image PowerUpImage;
	public Image GridCellImage;
	public Sprite PiercingSprite;
	public Sprite VortexSprite;
	public Sprite MoveSprite;
	public Sprite BounceSprite;
	public Sprite InstantSprite;
	public Sprite BombSprite;
	public Sprite PowerUpSprite;
	public Sprite CurrencySprite;
	public Sprite AmmoSprite;
	public Sprite TauntSprite;
	public Sprite InfectedSprite;
	public Sprite PortalSprite;
	public Sprite EmptyggSprite;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OnClick()
	{
		FindObjectOfType<LevelCreationMenu> ().GridCellClicked(this);
	}

	public void UpdateInformation()
	{
		
		WidgetHealth.text = Cell.bombValue.ToString ();
		WidgetCellType.text = Cell.cellType.ToString ();
		WidgetPowerUp.text = Cell.powerup.ToString ();
		if (Cell.cellType == GridCell.type.Currency) {
			GridCellImage.sprite = CurrencySprite;
		} else if (Cell.cellType == GridCell.type.Powerup) {
			GridCellImage.sprite = PowerUpSprite;
		} else if (Cell.cellType == GridCell.type.Bomb) {
			GridCellImage.sprite = BombSprite;
		} else if (Cell.cellType == GridCell.type.Ammo) {
			GridCellImage.sprite = AmmoSprite;
		} else if (Cell.cellType == GridCell.type.Taunt) {
			GridCellImage.sprite = TauntSprite;
		} else if (Cell.cellType == GridCell.type.Infected) {
			GridCellImage.sprite = InfectedSprite;
		} else if (Cell.cellType == GridCell.type.Portal) {
			GridCellImage.sprite = PortalSprite;
		}
		else {
			GridCellImage.sprite = EmptyggSprite;
		}			
			
		if (Cell.powerup == GridCell.powerupType.DoubleDamage) {
			PowerUpImage.sprite = VortexSprite;
		} else if (Cell.powerup == GridCell.powerupType.InstantBomb) {
			PowerUpImage.sprite = InstantSprite;
		} else if (Cell.powerup == GridCell.powerupType.Mirror) {
			PowerUpImage.sprite = BounceSprite;
		} else if (Cell.powerup == GridCell.powerupType.Piercing) {
			PowerUpImage.sprite = PiercingSprite;
		} else if (Cell.powerup == GridCell.powerupType.ShootNMove) {
			PowerUpImage.sprite = MoveSprite;
		} else {
			PowerUpImage.sprite = EmptyggSprite;
		}
		if (Cell.cellType == GridCell.type.Currency) {
			PowerUpImage.sprite = EmptyggSprite;
		} else if (Cell.cellType == GridCell.type.Ammo) {
			PowerUpImage.sprite = EmptyggSprite;
		}

	}
}
